#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 26 14:13:45 2021

@author: pmarie
"""
import numpy as np
import scipy.interpolate as inter
import matplotlib.pyplot as pplot

Xi = np.array([1,2,3,5])
Yi = np.array([1,4,2,5])

def Neville(Tx,Ty,x) :
    dim = np.shape(Tx)[0]
    matCoeff = np.zeros(shape=(dim,dim))  
    matCoeff[:,0] = Ty
    for j in range(1,dim) :
        for i in range(0,dim-j) :
            matCoeff[i,j] = ((Tx[i]-x)*matCoeff[i+1,j-1]+(x-Tx[i+j])*matCoeff[i,j-1])/(Tx[i]-Tx[i+j])
    return matCoeff[0,dim-1]

pplot.scatter(Xi,Yi,c='blue')
xplot = np.linspace(0,6,150)
yplot = np.array([Neville(Xi,Yi,x) for x in xplot])
pplot.plot(xplot,yplot,color='green')
fct = inter.interp1d(Xi,Yi,fill_value="extrapolate",kind="cubic")
yplot2 = fct(xplot)
pplot.plot(xplot,yplot2,"--",color="black") 
pplot.show()

