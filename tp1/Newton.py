#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 26 15:35:13 2021

@author: pmarie
"""


import numpy as np
import matplotlib.pyplot as pplot

def diffDivis(Xi,Yi):
    size = np.size(Xi)
    tabdf= Yi.copy()
    for i in range(size):
        for j in range(size-1,i,-1):
            tabdf[j]=(tabdf[j]-tabdf[j-1])/(Xi[j]-Xi[j-i-1])
    return tabdf

def Newton (Xi,tabdf,linspace) :
    size=np.size(Xi)
    p=tabdf[size-1]
    for i in range(size-1,0,-1):
        p=tabdf[i-1]+p*(linspace-Xi[i-1])
    return p

Xi = np.array([1,2,3,4,5,6,7,8,9,10,11,12])
Yi = np.array([8.6,7,6.4,4,2.8,1.8,1.8,2.3,3.2,4.7,6.2,7.9])
x = np.linspace(1,12,5000)
pplot.plot(x,Newton(Xi,diffDivis(Xi,Yi),x))
Xi = np.array([1,2,4,5,6,7,8,9,10,11,12])
Yi = np.array([8.6,7,4,2.8,1.8,1.8,2.3,3.2,4.7,6.2,7.9])
x = np.linspace(1,12,5000)
pplot.plot(x,Newton(Xi,diffDivis(Xi,Yi),x))
pplot.show()
        