# Calcul naïf d'une spline cubique périodique
# On se donne n+1=3 points (x_i,y_i) ainsi que la longueur d'une période p
# On cherche la spline périodique, de période p, dont le graphe passe par les points
#
# Pour cela, on duplique le premier point (x_0,y_0) en (x_0+p,y_0) (on arrive à n+1=4 points)
# On résout un système de 4*n équations linéaires à 4*n inconnues.

import math
import numpy as np
import scipy.linalg as nla
import matplotlib.pyplot as plt

# Les n+1=3 points initiaux
Tx = np.array ([1,3,4], dtype=np.float64)
Ty = np.array ([3,2,5], dtype=np.float64)

# La période
p = 6 

# On duplique le point (x_0,y_0) = (1,3) en (x_3,y_3) = (1+p,3) = (5,3)
# On a donc n+1=4 points à traiter

Tx = np.array ([1,3,4,Tx[0]+p], dtype=np.float64)
Ty = np.array ([3,2,5,Ty[0]], dtype=np.float64)

n = 3
A = np.zeros ([4*n,4*n], dtype=np.float64)
b = np.zeros (4*n, dtype=np.float64)

# le graphe de s_0 passe par (x_i,y_i) pour i = 0,1
A[0:2,0:4] = np.array ([[x**3, x**2, x, 1] for x in Tx[0:2]])
b[0:2] = Ty[0:2]

# le graphe de s_1 passe par (x_i,y_i) pour i = 1,2
A[2:4,4:8] = np.array ([[x**3, x**2, x, 1] for x in Tx[1:3]])
b[2:4] = Ty[1:3]

# le graphe de s_2 passe par (x_i,y_i) pour i = 2,3
A[4:6,8:12] = np.array ([[x**3, x**2, x, 1] for x in Tx[2:4]])
b[4:6] = Ty[2:4]

# Variante d'interpolation de Hermite
# s'_0(x) = s'_1(x) en x = x_1
A[6,0:8] = np.array ([[3*x**2, 2*x, 1, 0, -3*x**2, -2*x, -1, 0] for x in [Tx[1]]])
b[6] = 0

# s'_1(x) = s'_2(x) en x = x_2
A[7,4:12] = np.array ([[3*x**2, 2*x, 1, 0, -3*x**2, -2*x, -1, 0] for x in [Tx[2]]])
b[7] = 0

# Les dérivées secondes
# s''_0(x) = s''_1(x) en x = x_1
A[8,0:8] = np.array ([[6*x, 2, 0, 0, -6*x, -2, 0, 0] for x in [Tx[1]]])
b[8] = 0

# s''_1(x) = s''_2(x) en x = x_2
A[9,4:12] = np.array ([[6*x, 2, 0, 0, -6*x, -2, 0, 0] for x in [Tx[2]]])
b[9] = 0

# Les conditions de spline périodique :
# s'_0(x) = s'_2(x) en x = x_0
A[10,:] = np.array ([[3*Tx[0]**2, 2*Tx[0], 1, 0, 0, 0, 0, 0, -3*Tx[3]**2, -2*Tx[3], -1, 0]])
b[10] = 0

# s''_0(x) = s''_2(x) en x = x_0
A[11,:] = np.array ([[6*Tx[0], 2, 0, 0, 0, 0, 0, 0, -6*Tx[3], -2, 0, 0]])
b[11] = 0

a = nla.solve (A, b)

def s (x0) :
# On ramène x dans l'intervalle [Tx[0],Tx[0]+p]
    x = x0 - math.floor ((x0-Tx[0])/p)*p
    if x < Tx[1] :
        return ((a[0]*x + a[1])*x + a[2])*x + a[3]
    elif x < Tx[2] :
        return ((a[4]*x + a[5])*x + a[6])*x + a[7]
    else :
        return ((a[8]*x + a[9])*x + a[10])*x + a[11]

plt.scatter (Tx, Ty)
h = .5

xplot = np.linspace (Tx[0]-.1*h, Tx[0]+3*p+.1*h, 100)
yplot = [s(x) for x in xplot]
plt.plot (xplot,yplot)
plt.show ()


