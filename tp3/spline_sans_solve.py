#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 15 9:50:25 2021

@author: pmarie
"""
import numpy as np
import matplotlib.pyplot as pplot
import extraire_hispanic as extH
from math import sqrt

#TP3 : sans solve
### On implémente l'algorithme de calcul d'une spline lissante

x = extH.x
y = extH.y
n = extH.n

#on fixe p à 0.2
p=0.2

def h(x):
    Hi = np.zeros(n)
    for i in range(n) :
        Hi[i] = x[i+1]-x[i]
    return Hi

Hi = h(x)
T = np.zeros((n-1,n-1))

#Contruction de la diagonale de T
for i in range(n-1):
    T[i,i] = 2*(Hi[i]+Hi[i+1])
#construction de la sous-diagonale et de la sur-diagonale de T    
for i in range(n-2):    
    T[i+1,i] = Hi[i+1]
    T[i,i+1] = Hi[i+1]
    
T = (1/3)*T

Gi = 1/Hi

Q = np.zeros((n+1,n-1))

for i in range(n-1) :
    Q[i,i] = Gi[i]
    
for i in range(n-1):
    Q[i+1,i] = -Gi[i]-Gi[i+1]
    Q[i+2,i] = Gi[i+1]
    
    
sg = np.eye(n+1)

A = np.dot(np.dot(np.transpose(Q),sg),Q)+p*T
b = p*np.dot(np.transpose(Q),y)

#On calcul C avec l'algorithme de Cholesky 
#On présume que la matrice A est symétrique et définie positive
#Renvoi la matrice L de la factorisation A = L.Lt
def cholesky(A):
    n = len(A)
    L = [[0.0] * n for i in range(n)]
    for i in range(n):
        for r in range(i+1):
            S= sum(L[i][j] * L[r][j] for j in range(r))
            
            if (i == r):
                L[i][r] = sqrt(A[i][i] - S)
            else:
                L[i][r] = (1.0 / L[r][r] * (A[i][r] - S))
    return L

L = cholesky(A) 
#)Calcul de c
c = np.dot(np.dot(np.linalg.inv(np.transpose(L)),np.linalg.inv(L)),b)


#On refait les calculs des a, b et d comme précedemment ainsi que la fonction eval_spline
a = y-(1/p)*np.dot(np.dot(sg,Q),c)

c=np.insert(c,0,0)
c=np.append(c,0)
 
d = np.zeros(n)

for i in range(n):
    d[i]= (c[i+1]-c[i])/(3*Hi[i])
 
b = np.zeros(n)
 
for i in range(n):
    b[i] = ((a[i+1]-a[i])/Hi[i])-c[i]*Hi[i]-d[i]*(Hi[i]**2)


def eval_spline(z):
    i = np.searchsorted(x,z)-1
    fct = a[i]+b[i]*(z-x[i])+c[i]*((z-x[i])**2)+d[i]*((z-x[i])**3)
    return fct

pplot.scatter(x,y)
Xi = np.linspace(9.71,25.7,10000)
Yi = eval_spline(Xi)
pplot.plot(Xi,Yi,color="red")
pplot.show()
