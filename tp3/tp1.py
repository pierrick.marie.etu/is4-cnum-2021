#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 10 15:11:35 2021

@author: pmarie
"""
import numpy as np
import extraire_hispanic as extract


x=extract.x
y=extract.y
n=extract.n

def h(x):
    H=np.zeros(n)
    for i in range(n):
        H[i]=x[i+1]-x[i]
    return H

H=h(x)

T=np.zeros(n-1,n-1)
for i in range(n-1):
    T[i,j]=2*(H[i]+H[i+1])
for j in range(n-2):
              