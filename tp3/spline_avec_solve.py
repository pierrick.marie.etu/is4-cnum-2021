#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 17 13:50:27 2021

@author: pmarie
"""
import numpy as np
import scipy.linalg as scp
import matplotlib.pyplot as pplot
import extraire_hispanic as extH

# TP3  : avec solve
### On implémente les variables necessaires au calcul de la spline lissante à l'aide des variables de extraire_espanic


x = extH.x
y = extH.y
n = extH.n

#On fixe p à 0.2
p=0.2

#Création de la fonction h qui remplit un vecteur avec les coefficients hi
def h(x):
    Hi = np.zeros(n)
    for i in range(n) :
        Hi[i] = x[i+1]-x[i]
    return Hi

Hi = h(x)
T = np.zeros((n-1,n-1))

#Contruction de la diagonale de T
for i in range(n-1):
    T[i,i] = 2*(Hi[i]+Hi[i+1])
#construction de la sous-diagonale et de la sur-diagonale de T    
for i in range(n-2):    
    T[i+1,i] = Hi[i+1]
    T[i,i+1] = Hi[i+1]
    
T = (1/3)*T

Gi = 1/Hi

#initialisation de la matrice Q avec les dimensions
Q = np.zeros((n+1,n-1))

#remplissage de Q
for i in range(n-1) :
    Q[i,i] = Gi[i]
    
for i in range(n-1):
    Q[i+1,i] = -Gi[i]-Gi[i+1]
    Q[i+2,i] = Gi[i+1]
    
    
#définition de l'écart type
sg = np.eye(n+1)


#Calcul de A et B
A = np.dot(np.dot(np.transpose(Q),sg),Q)+p*T
B = p*np.dot(np.transpose(Q),y)
#Résolution du système avec solve
c = scp.solve(A,B)





#Calcul de a
a = y-(1/p)*np.dot(np.dot(sg,Q),c)

#modification de c pour ajouter les conditions voulues
c=np.insert(c,0,0)
c=np.append(c,0)

#Calcul de b
b = np.zeros(n)

#Calcul de d
d = np.zeros(n)

for i in range(n):
    d[i]= (c[i+1]-c[i])/(3*Hi[i])


 
for i in range(n):
    b[i] = ((a[i+1]-a[i])/Hi[i])-c[i]*Hi[i]-d[i]*(Hi[i]**2)

#Definition de eval_spline qui retorune la valeur de la spline en z
def eval_spline(z):
    i = np.searchsorted(x,z)-1
    fct = a[i]+b[i]*(z-x[i])+c[i]*((z-x[i])**2)+d[i]*((z-x[i])**3)
    return fct

#Représentation graphique de la spline
pplot.scatter(x,y)
Xi = np.linspace(9.71,25.69,3000)
Yi = eval_spline(Xi)
pplot.plot(Xi,Yi,color="red")
pplot.show()





    
