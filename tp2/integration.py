#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Dec  8 17:15:47 2021

@author: pmarie
"""
import numpy as np
import matplotlib.pyplot as pplot  

#TP2
#Question 1 : fonction qui retourne l'erreur relative
def relerr(I,Iprime) :
    return abs(I-Iprime)/abs(I)

#Question 2 : approximation du nombre de bits de I'
def nbbits(I,Iprime) :
    return -(np.log2(relerr(I,Iprime)))

#Question 3 : On réalise le calcul manuellement pour vérifier les calculs précédents
print(relerr(1024,1023))
print(nbbits(1024,1023))
#Mêmes résultats : fonctions correctes

#Question 4 :

#codage de f1
def f1(x):
    t1 = 2+x
    t2=np.log(t1)
    t3=t2**2
    t7=np.sqrt(x)
    t9=np.cos(t7)
    return ((t9/(2*t7))-(1/(t3*t1)))*p1(x)

#codage de p1
def p1(x) :
    t2=np.log(2+x)
    t4=np.sqrt(x)
    t5=np.sin(t4)
    return np.exp(1/t2+t5+1)
 
     


#Question 5 :
    
#codage de f2
def f2(x):
    return 2/(2*(x**2)-2*x+1)

#codage de p2
def p2(x):
    return 2*np.arctan(2*x-1)

#Question 6 :
#fonction trapèzes retournant l’approximation de l’intégrale de f entre a et b
def Trapezes(f,a,b,N):
    h = (b-a)/N
    Xi = np.linspace(a,b,N+1)
    somme = f(Xi[0])/2
    for i in range(1,N):
        somme += f(Xi[i])
    somme += f(Xi[N])/2
    return h*somme

#Question 7
#Fonction simpson
def Simpson(f,a,b,N):
    h = (b-a)/N
    Xi = np.linspace(a,b,N+1)
    ordre = 4
    somme = f(Xi[0])
    for i in range(1,N):
        if ordre == 4 :
            somme += 4*f(Xi[i])
            ordre = 2
        else :
            somme += 2*f(Xi[i])
            ordre = 4
    somme += f(Xi[N])
    return (h/3)*somme

#Question 8
print("Avec les fonctions on a :")
print(Trapezes(f1,5,10,50),Simpson(f1,5,10,50))
print(Trapezes(f2,5,10,50),Simpson(f2,5,10,50))

print("Experimentalement on a  :")
print(p1(10)-p1(5),p2(10)-p2(5))
#On obtient bien les mêmes résulats

#Question 9
ps = [i for i in range(2,16)]
Xi = np.array([2**i for i in ps])
def TrapezesB (f,a,b,p):
    Bi = np.array([])
    for i in range(0,np.size(Xi)) :
        Iprime = Trapezes(f,a,b,Xi[i])
        Bi = np.append(Bi,nbbits(p(b)-p(a),float(Iprime)))
    return Bi

def SimpsonB (f,a,b,p):
    Bi = np.array([])
    for i in range(0,np.size(Xi)) :
        Iprime = Simpson(f,a,b,Xi[i])
        Bi = np.append(Bi,nbbits(p(b)-p(a),float(Iprime)))
    return Bi


pplot.scatter(np.array(ps),TrapezesB(f1,5,10,p1),color ="blue",label="Trapezes/f1")
pplot.scatter(np.array(ps),TrapezesB(f2,5,10,p2),color="black",label="Trapezes/f2")
pplot.scatter(np.array(ps),SimpsonB(f1,5,10,p1),color="green",label="Simpson/f1")
pplot.scatter(np.array(ps),SimpsonB(f2,5,10,p2),color="red",label="Simpson/f2")
pplot.ylabel("Nombre de bits de I'")
pplot.xlabel("Nombre de pas N")
pplot.legend()
pplot.show()

#La pente représente le nombre de bits gagné lorsque l'on multiplie le pas par 2
# La cause du nombre de bit qui n'augmente plus est la capacité des ordinateurs. 


           



    
